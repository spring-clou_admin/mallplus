package com.zscat.mallplus.pms.service;

import com.zscat.mallplus.pms.entity.PmsGiftsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 帮助分类表 服务类
 * </p>
 *
 * @author zscat
 * @since 2019-07-07
 */
public interface IPmsGiftsCategoryService extends IService<PmsGiftsCategory> {

}
