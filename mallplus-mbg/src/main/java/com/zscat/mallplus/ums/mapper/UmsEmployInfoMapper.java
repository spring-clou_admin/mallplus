package com.zscat.mallplus.ums.mapper;

import com.zscat.mallplus.ums.entity.UmsEmployInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-07-02
 */
public interface UmsEmployInfoMapper extends BaseMapper<UmsEmployInfo> {

}
